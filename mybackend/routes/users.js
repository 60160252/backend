const express = require('express')
const router = express.Router()
const usersController = require('../controller/UsersController')


/* GET users listing. */
  // res.json(usersController.getUsers())
  // pattern 1
  // User.find({}).exec(function (err, users) {
  //     if (err) {
  //       res.status(500).send()
  //     }
  //     res.json(users)
  // })

  // pattern 2 : promise
  // User.find({}).then(function (users) {
  //   res.json(users)
  // }).catch(function (err) {
  //   res.status(500).send(err)
  // })
router.get('/', usersController.getUsers)

router.get('/:id', usersController.getUser)

router.post('/', usersController.addUser)

router.put('/', usersController.updateUser)

router.delete('/:id', usersController.deleteUser)

module.exports = router
