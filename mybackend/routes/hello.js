const express = require('express')
const router = express.Router()

router.get('/:message', (req, res, next) => {
  const { params } = req
  res.json({ message: 'Hello My name is zhou!', params })
})

module.exports = router
